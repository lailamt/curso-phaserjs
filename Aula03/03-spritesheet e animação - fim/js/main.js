var game=new Phaser.Game(800, 600, Phaser.CANVAS, 'game', {preload: preload, create: create, update: update});

function preload(){
  //game.load.spritesheet('frutas', 'assets/images/frutas.png', 32, 32);
  //game.load.spritesheet('chicken', 'assets/images/chick.png', 16, 18);
  game.load.spritesheet('jogador', 'assets/images/player.png', 16, 21);
}
function create(){
  game.stage.backgroundColor = "9370DB";
  game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true;

  player = game.add.sprite(50,50, 'jogador');
  player.anchor.set(.5);
  player.scale.set(1);
  player.animations.add('walkDown', [0,1,2,3], 10, false);
  player.animations.add('walkUp', [8,9,10,11], 10, false);
  player.animations.add('walkRight', [4,5,6,7], 12, false);
  player.animations.add('walkLeft', [12,13,14,15], 12, false)

    /*
  for(var i=0;i<100;i++){
    posx = game.rnd.between(0, game.world.width-32);
    posy = game.rnd.between(0, game.world.height-32); 
    nfruta = game.rnd.between(0,9);

    game.add.sprite(posx, posy, 'frutas', nfruta);
  }
  
  chicken = game.add.sprite(50, 50, 'chicken');
  chicken.anchor.set(.5);
  chicken.scale.set(1.5);
  chicken.animations.add('walk', [0,1,2], 10, false);
  */

  cursor = game.input.keyboard.createCursorKeys();
}
function update(){
  //chicken.animations.play('walk');
  
  if(cursor.down.isDown){
    player.y += 3; 
    //chicken.scale.x = 1.5;
    player.animations.play('walkDown');
    if(player.y>=600){
        player.y=0;
    }
  }
  else if(cursor.right.isDown){
    player.x += 3; 
    //chicken.scale.x = -1.5;
    player.animations.play('walkRight');
    if(player.x>=800){
        player.x=0;
    }
  }
  else if(cursor.up.isDown){
    player.y -= 3;
    player.animations.play('walkUp');
    if(player.y<=0){
        player.y=620;
    }
  }else if(cursor.left.isDown){
    player.x -= 3;
    player.animations.play('walkLeft');
    if(player.x<=0){
        player.x=820;
    }
  }else{
    player.frame = 0;
  }
}
