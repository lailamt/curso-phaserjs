var game=new Phaser.Game(320, 320, Phaser.CANVAS, 'Example', {preload: preload, create: create, update: update});

function preload(){
    game.load.spritesheet('player', 'assets/images/player.png', 16, 21);
  
  game.load.tilemap("mapa1","assets/tilemaps/mapa1.json",null, Phaser.Tilemap.TILED_JSON);
  game.load.image('overworld','assets/tilemaps/overworld.png');
}

function create(){
  mapa1 = game.add.tilemap('mapa1');
  mapa1.addTilesetImage('overworld');
  mapa1.createLayer('fundo');
  mapa1.createLayer('ruas');
  elementos = mapa1.createLayer('elementos');
  
  mapa1.setCollisionBetween(1,900,true,'elementos');
    
  player = game.add.sprite(70, 125, 'player');
  player.anchor.setTo(0.5);
  player.frame=4;
  player.animations.add('walkD', [0, 1, 2, 3], 10, false);
  player.animations.add('walkR', [4, 5, 6, 7], 10, false);
  player.animations.add('walkU', [8, 9, 10, 11], 10, false);
  player.animations.add('walkL', [12, 13, 14, 15], 10, false);
  cursors = game.input.keyboard.createCursorKeys();

  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.enable(player);
  
  player.body.setSize(14,5,1,16);
    
   

}
function update(){
  game.physics.arcade.collide(player,elementos);
  
  player.body.velocity.setTo(0);

  if (cursors.up.isDown)
  {
      player.animations.play('walkU');
      player.body.velocity.y = -128;
  }
  else if (cursors.down.isDown)
  {
      player.animations.play('walkD');
      player.body.velocity.y = 128;
  } 
  else if (cursors.left.isDown)
  {
      player.animations.play('walkL');
      player.body.velocity.x = -128;
  }
  else if (cursors.right.isDown)
  {
      player.animations.play('walkR');
      player.body.velocity.x = 128;
  }
  else
      player.frame = 0;


}
