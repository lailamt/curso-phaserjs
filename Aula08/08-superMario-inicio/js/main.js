var game=new Phaser.Game(360, 240, Phaser.CANVAS, 'Example', {preload: preload, create: create, update: update});

function preload(){
  game.load.spritesheet('player', 'assets/images/player.png', 16, 21);
  
  game.load.tilemap('level1','assets/tilemaps/level1.json', null, Phaser.Tilemap.TILED_JSON);
  
  game.load.image('tileset','assets/tilemaps/tileset.png');

}

function create(){
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true;
  game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  
  
  
  cursors = game.input.keyboard.createCursorKeys();
  
  spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  spaceKey.onDown.add(pula);

  
  // criação do mapa de tiles
  level = game.add.tilemap('level1');
  level.addTilesetImage('tileset');    
  
  // criação das camadas
  fundo = level.createLayer('fundo');
  nuvens = level.createLayer('nuvem');  
  colisao = level.createLayer('colisao');
  moedas = level.createLayer('colisaomoeda');
  
  // definição do scrolling
  fundo.scrollFactorX= .5;
  nuvens.scrollFactorX= .2;
  
  // definição das colisões 
  level.setCollisionBetween(1, 40, true, 'colisao'); 
  level.setCollisionBetween(1, 40, true, 'colisaomoeda');
  

  player = game.add.sprite(30, 79, 'player');
  player.anchor.setTo(0.4);
  
  player.frame=4;
  player.animations.add('walkD', [0, 1, 2, 3], 10, false);
  player.animations.add('walkR', [4, 5, 6, 7], 10, false);
  player.animations.add('walkU', [8, 9, 10, 11], 10, false);
  player.animations.add('walkL', [12, 13, 14, 15], 10, false);

  
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.enable(player);

  // gravidade
  player.body.gravity.y = 2000;

  game.world.setBounds(0, 0, 640, 240); // limites do mundo
  player.body.collideWorldBounds = true; // colisão com os limites
  game.camera.follow(player); // foco no sprite principal
  
}

function pula(){
  if(player.body.onFloor()){
    player.body.velocity.y = -550;
  }
}

function getMoeda(player, tile){
  if(tile.index==11){   
  level.removeTile(tile.x,tile.y,'colisaomoeda');
  }
}

function update(){
 // definir as colisões
 game.physics.arcade.collide(player, colisao);
 game.physics.arcade.overlap(player, moedas, getMoeda);
 
  
  if (cursors.left.isDown)
  {
      player.animations.play('walkL');
      player.body.velocity.x = -128;
  }
  else if (cursors.right.isDown)
  {
      player.animations.play('walkR');
      player.body.velocity.x = 128;
  }
  else{
    player.body.velocity.x = 0;
    player.frame = 0;
  }

}