var game=new Phaser.Game(500, 340, Phaser.CANVAS, 'game', {preload: preload, create: create, update: update});

function preload(){
    game.load.image('forest', 'assets/images/forest.png');    //imagem estática
    game.load.spritesheet('galinha', 'assets/images/chick.png', 16, 18);    //sprite animado
    game.load.spritesheet('frutas', 'assets/images/frutas.png', 32,32);
    game.load.image('platform', 'assets/images/wallWoodH.png', 16, 18);    //sprite animado
    game.load.audio('somPulo', ['assets/audio/jump.mp3', 'assets/audio.jump.ogg']);
    game.load.audio('morreu', ['assets/audio/dead.mp3', 'assets/audio.dead.ogg']);
    game.load.audio('comeu', ['assets/audio/eat.mp3', 'assets/audio.eat.ogg'])
}
function create(){    
    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.add.image(0, 0, 'forest');

    
    morango = game.add.sprite(250, 100, 'frutas', 9);
    morango.scale.set(.3);
    game.physics.enable(morango);
    morango.body.gravity.y = 980;
    morango.body.mass = -10;
  
    // criação galinha
    galinha = game.add.sprite(250, 190, 'galinha');
    galinha.animations.add('walk', [0,1,2], 10, false);
    galinha.anchor.setTo(0.5);
    galinha.scale.setTo(1);

    
    //criação dos sons
    somPulo = game.add.audio('somPulo');
    morreu = game.add.audio('morreu');
    comeu = game.add.audio('comeu');
    
    //carregando as plataformas
    p1 = game.add.sprite(100, 200, 'platform');
    p2 = game.add.sprite(250, 150, 'platform');
    p3 = game.add.sprite(300, 100, 'platform');
    rect = new Phaser.Rectangle(0,0,50,p2.height);  //cria um retângulo
    p2.crop(rect);  //corta a plataforma com o formato do retângulo de cima
    p3.crop(new Phaser.Rectangle(0, 0, 150, p3.height));
    
    //faz a galinha pular pela parte da frente da plataforma
    game.world.bringToTop(galinha);

    //fisica do jogo
    game.physics.enable([galinha, p1, p2, p3]);   //inicia a física nos elementos
    p2.body.checkCollision.down=false;    //nessa plataforma desativa colisão por baixo

    galinha.body.gravity.y = 2000;   //aplica gravidade a galinha

    // input teclado
    cursors = game.input.keyboard.createCursorKeys();
}
function update(){
  if(galinha.inWorld==false){
    galinha.position.set(250, 175);
    morreu.play();
  }

  //inicio da aplicação da física
  game.physics.arcade.collide(galinha,[p1,p2,p3]);
  game.physics.arcade.collide(morango,[p1,p2,p3]);
  //game.physics.arcade.collide(morango,galinha);
  game.physics.arcade.overlap(galinha,morango, comer);
  p1.body.immovable = (true);
  p2.body.immovable = (true);
  p3.body.immovable = (true);

  // movimento
  if(cursors.right.isDown){
    galinha.body.velocity.x = 150; 
    galinha.scale.x = 1;
    galinha.animations.play('walk');
  }
  else if(cursors.left.isDown){
    galinha.body.velocity.x = -150; 
    galinha.scale.x = -1;
    galinha.animations.play('walk');
  }

  else{
    galinha.body.velocity.x = 0;
    galinha.frame = 0;
    //galinha.body.velocity.y = 100;
  }
  if(cursors.up.isDown && galinha.body.touching.down){
    somPulo.play();
    galinha.body.velocity.y = -600;
  }
  if(!galinha.body.touching.down){
    galinha.frame = 3;
  }

  function comer(galinha, morango){
    morango.kill();
    comeu.play();
  }
}
