var game=new Phaser.Game(500, 340, Phaser.CANVAS, 'game', {preload: preload, create: create, update: update});

function preload(){
    game.load.image('forest', 'assets/images/forest.png');    //imagem estática
    game.load.spritesheet('galinha', 'assets/images/chick.png', 16, 18);    //sprite animado
    game.load.image('platform', 'assets/images/wallWoodH.png', 16, 18);    //sprite animado
    game.load.audio('somPulo', ['assets/audio/jump.mp3', 'assets/audio.jump.ogg'])
    game.load.audio('morreu', ['assets/audio/dead.mp3', 'assets/audio.dead.ogg'])
}
function create(){    
    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.add.image(0, 0, 'forest');
  
    // criação galinha
    galinha = game.add.sprite(250, 190, 'galinha');
    galinha.animations.add('walk', [0,1,2], 10, false);
    galinha.anchor.setTo(0.5);
    galinha.scale.setTo(1);

    //criação do som do pulo
    somPulo = game.add.audio('somPulo');
    morreu = game.add.audio('morreu');

    //carregando as plataformas
    p1 = game.add.sprite(100, 200, 'platform');
    p2 = game.add.sprite(350, 150, 'platform');

    //fisica do jogo
    game.physics.enable([galinha, p1, p2]);   //inicia a física nos elementos
    galinha.body.gravity.y = 2000;   //aplica gravidade a galinha

    // input teclado
    cursors = game.input.keyboard.createCursorKeys();
}
function update(){
  if(galinha.inWorld==false){
    galinha.position.set(250, 175);
    morreu.play();
  }

  //inicio da aplicação da física
  game.physics.arcade.collide(galinha,[p1,p2]);
  p1.body.immovable = (true);
  p2.body.immovable = (true);

  // movimento
  if(cursors.right.isDown){
    galinha.body.velocity.x = 150; 
    galinha.scale.x = 1;
    galinha.animations.play('walk');
  }
  else if(cursors.left.isDown){
    galinha.body.velocity.x = -150; 
    galinha.scale.x = -1;
    galinha.animations.play('walk');
  }

  else{
    galinha.body.velocity.x = 0;
    galinha.frame = 0;
    //galinha.body.velocity.y = 100;
  }
  if(cursors.up.isDown && galinha.body.touching.down){
    somPulo.play();
    galinha.body.velocity.y = -600;
  }
  if(!galinha.body.touching.down){
    galinha.frame = 3;
  }
}
