var game=new Phaser.Game(500, 340, Phaser.CANVAS, 'game', {preload: preload, create: create, update: update});

function preload(){
    game.load.image('forest', 'assets/images/forest.png');    //imagem estática
    game.load.spritesheet('galinha', 'assets/images/chick.png', 16, 18);    //sprite animado
    game.load.spritesheet('morango', 'assets/images/morango.png', 32,32);
    game.load.image('platform', 'assets/images/wallWoodH.png', 16, 18);    //sprite animado
    game.load.audio('somPulo', ['assets/audio/jump.mp3', 'assets/audio.jump.ogg']);
    game.load.audio('morreu', ['assets/audio/dead.mp3', 'assets/audio.dead.ogg']);
    game.load.audio('comeu', ['assets/audio/eat.mp3', 'assets/audio.eat.ogg'])
}

function create(){    
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true;
  game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.add.image(0, 0, 'forest');
    
  //criando elementos
    //criação da galinha
    galinha = game.add.sprite(250, 190, 'galinha');
    galinha.animations.add('walk', [0,1,2], 10, false);
galinha.anchor.setTo(0.5);
    galinha.scale.setTo(1);
    //criação dos morangos
    morangos = game.add.group();
    morangos.create(400, 10, 'morango');
    morangos.create(200, 10, 'morango');
    //criação das plataformas
    plataformas = game.add.group()
    plataformas.create(100, 200, 'platform')
    plataformas.create(250, 150, 'platform')
    plataformas.create(300, 100, 'platform')
    //carregando as plataformas
    //const [p1, p2, p3] = plataformas.children -> copia os elementos do array plataformas
    //                                          -> referencia os elementos a cada posição do array criado
    //p3.crop(new Phaser.Rectangle(0, 0, 150, p3.height));
    plataformas.children[1].crop(new Phaser.Rectangle(0, 0, 100, plataformas.children[1].height));
    //criação dos sons
    somPulo = game.add.audio('somPulo');
    morreu = game.add.audio('morreu');
    comeu = game.add.audio('comeu');
 
  //criando a física dos elementos
    //física da galinha
    game.physics.enable(galinha);
    game.world.bringToTop(galinha);   //faz a galinha pular pela parte da frente da plataforma
    galinha.body.gravity.y = 2000;   //aplica gravidade a galinha
    //física dos morangos
    game.physics.enable(morangos);
    morangos.callAll('scale.set', 'scale', .8, .8);
    morangos.setAll('body.gravity.y', 980);
    //física das plataformas
    game.physics.enable(plataformas);
    plataformas.setAll('body.immovable', true);
    plataformas.setAll('body.checkCollision.down', false);
    plataformas.setAll('body.checkCollision.left', false);
    plataformas.setAll('body.checkCollision.right', false);

  // input teclado
  cursors = game.input.keyboard.createCursorKeys();
}

function update(){
  if(galinha.inWorld==false){
    galinha.position.set(250, 175);
    morreu.play();
  }

  //inicio da aplicação da física
  game.physics.arcade.collide(galinha,plataformas);
  game.physics.arcade.collide(morangos,plataformas);
  game.physics.arcade.overlap(galinha,morangos,comer);
  
  // movimento
  if(cursors.right.isDown){
    galinha.body.velocity.x = 150; 
    galinha.scale.x = 1;
    galinha.animations.play('walk');
  }
  else if(cursors.left.isDown){
    galinha.body.velocity.x = -150; 
    galinha.scale.x = -1;
    galinha.animations.play('walk');
  }

  else{
    galinha.body.velocity.x = 0;
    galinha.frame = 0;
    //galinha.body.velocity.y = 100;
  }
  if(cursors.up.isDown && galinha.body.touching.down){
    somPulo.play();
    galinha.body.velocity.y = -600;
  }
  if(!galinha.body.touching.down){
    galinha.frame = 3;
  }

  function comer(galinha, morango){
    galinha.frame = 3;
    morango.kill();
    comeu.play();
  }
}