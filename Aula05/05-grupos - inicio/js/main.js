var game=new Phaser.Game(500, 340, Phaser.CANVAS, 'game', {preload: preload, create: create, update: update});

function preload(){
    game.load.image('forest', 'assets/images/forest.png');
    game.load.image('plataforma', 'assets/images/plataforma.png');
    game.load.spritesheet('galinha', 'assets/images/chick.png', 16, 18);
}
function create(){    
  // centralizar tela
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true;

  // tela cheia
  game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  game.add.image(0, 0, 'forest');
  
  // criação galinha e animação
  galinha = game.add.sprite(150, 100, 'galinha');
  galinha.animations.add('walk', [0,1,2], 10, false);
  galinha.anchor.setTo(0.5);
  galinha.scale.setTo(1.5);
  
  // ativamos a fisica
  game.physics.startSystem(Phaser.Physics.ARCADE);
  
  game.physics.enable(galinha);
  
  galinha.body.gravity.y = 980;
  
  p1 = game.add.sprite(100,230,'plataforma');
  game.physics.enable(p1);
  p1.body.immovable = true;
  
  p2 = game.add.sprite(250, 190,'plataforma');
  rect =new Phaser.Rectangle(0, 0, 50, p2.height)
  p2.crop(rect);
  
  p3 = game.add.sprite(350,150,'plataforma');
  
  
  game.physics.enable(p2);
  game.physics.enable(p3);
  
  p2.body.immovable = true;
  p3.body.immovable = true;
  

  // input teclado
  cursors = game.input.keyboard.createCursorKeys();
  
}
function update(){
  
  game.physics.arcade.collide(galinha,p1);
  game.physics.arcade.collide(galinha,p2);
  game.physics.arcade.collide(galinha,p3);
  
  
  if(!galinha.inWorld){
    galinha.position.set(150,200);    
  }

  // movimento
  if(cursors.right.isDown){
    galinha.body.velocity.x = 200; 
    galinha.scale.x = 1.5;
    galinha.animations.play('walk');
  }
  else if(cursors.left.isDown){
    galinha.body.velocity.x=-200;
    galinha.scale.x = -1.5;
    galinha.animations.play('walk');
  }
  else{
    galinha.frame = 0;
    galinha.body.velocity.x=0;
  }
  
  if(cursors.up.isDown && galinha.body.touching.down){
    galinha.body.velocity.y = - 400;
  }
  if(!galinha.body.touching.down)
    galinha.frame = 3;
}