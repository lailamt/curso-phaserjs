var game = new Phaser.Game(1000, 700, Phaser.AUTO, 'game',{preload:preload,create:create,update:update});

function preload(){

  game.load.spritesheet('bird','assets/images/birdshit.png',712,637);
  game.load.image('cloud1','assets/images/cloud-1.png');
  game.load.image('cloud2', 'assets/images/cloud-2.png');
  game.load.image('spike1', 'assets/images/spike B.png');
  game.load.image('spike2', 'assets/images/spike C.png');
}
function create(){
  game.physics.startSystem(Phaser.Physics.ARCADE);

  // full screen centered
  game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true;
  game.stage.backgroundColor  = "#8bd5f7";

  //criação do passarinho e animação
  bird=game.add.sprite(450,250,'bird', 1);
  bird.animations.add('fly', [0,1], 5, true);
  bird.animations.add('die', [2,3], 5, true);
  bird.anchor.setTo(.0);
  bird.scale.setTo(0.1);

  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.enable(bird);

  bird.body.gravity.y=900;

  pontos = 0;
  
  var estilo = { font: '30px Arial', fill: '#ffffff' };
  txtPontos = game.add.text(70, 20, 'Pontos: ' + pontos, estilo);
  txtPontos.anchor.set(.5)
  
  //var click = game.input.mouse.addKey(Phaser.mouse.LEFT_BUTTON);
  cursors = game.input.keyboard.createCursorKeys();
}
function update(){
  bird.animations.play('fly');
  if(cursors.up.isDown){
    bird.body.velocity.y=-300;
  }
}