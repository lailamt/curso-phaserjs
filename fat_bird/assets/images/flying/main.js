var game = new Phaser.Game(500, 340, Phaser.CANVAS, 'game', { preload: preload, create: create, update: update });

function preload() {
    game.load.image('forest', 'assets/images/forest.png');
    game.load.image('plataforma', 'assets/images/plataforma.png');
    game.load.spritesheet('galinha', 'assets/images/chick.png', 16, 18);

    // carregar o elemento de fogo
    game.load.image('fogo', 'assets/images/fogo.png');
}

function create() {
    // tela cheia e centralizada 
    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.add.image(0, 0, 'forest');
    musica = game.add.audio('musica');

    // criação galinha e animação
    galinha = game.add.sprite(150, 100, 'galinha');
    galinha.animations.add('walk', [0, 1, 2], 10, false);
    galinha.anchor.setTo(0.5);
    galinha.scale.setTo(1.5);

    // ativamos a fisica
    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.physics.enable(galinha);

    galinha.body.gravity.y = 980;

    plataformas = game.add.group();
    plataformas.create(100, 230, 'plataforma');
    plataformas.create(225, 190, 'plataforma');
    plataformas.create(350, 150, 'plataforma');

    game.physics.enable(plataformas);

    plataformas.setAll('body.checkCollision.down', false);
    plataformas.setAll('body.checkCollision.left', false);
    plataformas.setAll('body.checkCollision.right', false);
    plataformas.setAll('body.immovable', true);

    galinha.body.collideWorldBounds = true;


    // input teclado
    cursors = game.input.keyboard.createCursorKeys();

    game.world.bringToTop(galinha);

    //tecla espaço
    spacekey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    spacekey.onDown.add(criaFogo);

    //criação do pool de sprites
    fogos = game.add.group();
    fogos.createMultiple(10, 'fogo');
    game.physics.enable(fogos);
    fogos.setAll("checkWorldBounds", true);
    fogos.setAll('outOfBoundsKill', true);

    //inicialiação de variáveis
    pFogo = 0;
    fazendoFogo = false;

}

function criaFogo() {
    //fogo = game.add.sprite(galinha.x, galinha.y + 4, 'fogo');
    //game.physics.enable(fogo);

    fogo = fogos.getFirstDead();
    if (!fogo) return
    fogo.reset(galinha.x, galinha.y);

    if (galinha.scale.x > 0) {
        fogo.scale.x = -1;
        fogo.body.velocity.x = -450;
    } else {
        fogo.body.velocity.x = 450;
    }

    fazendoFogo = true;
    game.time.events.add(100, mudaFogo);
    game.time.events.add(100, function() { //função anônima, pode ser () =>, segundo Ivens é mais bonito
        fazendoFogo = false;
    });
}

function mudaFogo() {
    fazendoFogo = false;
}

function update() {

    game.physics.arcade.collide(galinha, plataformas);

    if (galinha.y > 320) {
        galinha.position.set(150, 200);
    }

    // movimento
    if (cursors.right.isDown) {
        galinha.body.velocity.x = 200;
        galinha.scale.x = 1.5;
        galinha.animations.play('walk');
    } else if (cursors.left.isDown) {
        galinha.body.velocity.x = -200;
        galinha.scale.x = -1.5;
        galinha.animations.play('walk');
    } else {
        if (fazendoFogo) { galinha.frame = 3; } else {
            galinha.frame = 0;
            galinha.body.velocity.x = 0;
        }
        if (cursors.up.isDown && galinha.body.touching.down) {
            galinha.body.velocity.y = -400;
        }
        if (!galinha.body.touching.down)
            galinha.frame = 3;

        if (spacekey.isDown) {
            if (pFogo <= game.time.now) {
                criaFogo();
                pFogo = game.time.now + 200;
            }
        }

    }
}