var game=new Phaser.Game(800, 464, Phaser.AUTO, 'Example', {preload: preload, create: create, update: update});

function preload(){
  game.load.image('fundo','assets/images/landscape.png');
  game.load.image('ovelha','assets/images/ovelha.png');
  game.load.image('sol','assets/images/sol.png');
  game.load.image('flor1','assets/images/flor1.png');
  game.load.image('flor2','assets/images/flor2.png');
}

function create(){
 game.add.image(0,0,'fundo');
  ovelha = game.add.sprite(500,200,'ovelha');
  ovelha.scale.set(0.5);
  
  sol = game.add.sprite(game.world.centerX,50,'sol');
  sol.scale.set(.3);
  sol.anchor.set(.5);
  
  for(i=0;i<50;i++){
    posX = game.rnd.between(20,780);
    posY = game.rnd.between(304,460);

    flor1 = game.add.sprite(posX,posY,'flor1');
    flor1.anchor.set(.5,1);
    flor1.scale.set(.6);
    
    posX = game.rnd.between(20,780);
    posY = game.rnd.between(304,460);

    flor2 = game.add.sprite(posX,posY,'flor2');
    flor2.anchor.set(.5,1);
    flor2.scale.set(.5);
    
  }
}
function update(){
 sol.angle++;
}