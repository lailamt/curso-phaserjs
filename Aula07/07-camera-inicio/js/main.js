var game=new Phaser.Game(600, 400, Phaser.CANVAS, 'Example', {preload: preload, create: create, update: update});

function preload(){
  game.load.spritesheet('player', 'assets/images/player.png', 16, 21);
  game.load.image('bg','assets/images/pyramid.jpg');
}

function create(){
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true;
  game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  game.world.setBounds(0, 0, 900, 725);
  game.camera.follow(player);
  
  player = game.add.sprite(50, 50, 'player');
  player.anchor.setTo(0.5);
  player.frame=4;
  player.animations.add('walkD', [0, 1, 2, 3], 10, false);
  player.animations.add('walkR', [4, 5, 6, 7], 10, false);
  player.animations.add('walkU', [8, 9, 10, 11], 10, false);
  player.animations.add('walkL', [12, 13, 14, 15], 10, false);
  player.body.collideWorldBounds = true;
  
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.enable(player);
  
  cursors = game.input.keyboard.createCursorKeys();

}

function update(){
  
  player.body.velocity.setTo(0);

  if (cursors.up.isDown)
  {
      player.animations.play('walkU');
      player.body.velocity.y = -128;
  }
  else if (cursors.down.isDown)
  {
      player.animations.play('walkD');
      player.body.velocity.y = 128;
  } 
  else if (cursors.left.isDown)
  {
      player.animations.play('walkL');
      player.body.velocity.x = -128;
  }
  else if (cursors.right.isDown)
  {
      player.animations.play('walkR');
      player.body.velocity.x = 128;
  }
  else
      player.frame = 0;
}
